#!/bin/bash

arq=""

function menu() {
    echo "1 - Escolher um arquivo .zip existente"
    echo "2 - Criar arquivo .zip"
    echo "3 - Sair"
}

function menu2() {
    echo "1 - Listar o conteúdo do arquivo .zip"
    echo "2 - Pré-visualizar algum arquivo compactado neste zip"
    echo "3 - Adicionar arquivos ao zip"
    echo "4 - Remover arquivos do zip"
    echo "5 - Extrair todo o conteúdo do zip"
    echo "6 - Extrair arquivos específicos do zip"
    echo "7 - Sair"
}

function existente() {
    read -p "Digite o caminho do arquivo zip: " arq_zip
    if [ -f "$arq_zip" ]; then
        echo "Arquivo escolhido: $arq_zip"
        arq="$arq_zip"
    else
        echo "Arquivo não encontrado."
        arq=""
    fi
}

function criar() {
    read -p "Digite o nome para o arquivo: " arq_nome
    if [ "$arq_nome" ];then
	    arq="$arq_nome"
	    arq=$(zip -r "$arq.zip" "$arq")
	    echo "Arquivo criado com sucesso."
    else
	    echo "Arquivo ou dirétorio não encontrado"

    fi
}

function listar() {
    unzip -l "$arq"
}

function pre_visualizar() {
    read -p "Digite o nome do arquivo para pré-visualizar: " pre
    unzip -p "$arq" "$pre"
}

function adicionar() {
    read -p "Arquivo para adicionar no zip: " add
    zip "$arq" "$add"
}

function remover() {
    read -p "Digite o nome do arquivo a ser removido: " rem
    zip -d "$arq" "$rem"
}

function extrai_tudo() {
    unzip "$arq"
}

function extrai_especifico() {
    read -p "Digite o nome do arquivo a ser extraído: " ex
    unzip "$arq" "$ex"
}

while true; do
    menu
    read -p "Digite uma opção: " op
    case $op in
        1) existente ;;
        2) criar ;;
        *) echo "Escolha inválida. Tente novamente." ;;
    esac

    while [ "$op" != "7" ]; do
        menu2
        read -p "Digite uma opção: " op
        [ "$op" == "7" ] && echo "Saindo..." && exit
        case $op in
            1) listar ;;
            2) pre_visualizar ;;
            3) adicionar ;;
            4) remover ;;
            5) extrai_tudo ;;
            6) extrai_especifico ;;
            7) echo "Saindo..." && exit ;;
            *) echo "Opção inválida. Tente novamente." ;;
        esac
    done
done

