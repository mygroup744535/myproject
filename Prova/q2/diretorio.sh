#!/bin/bash

read -p "Diretório : " d1
read -p "Diretório 2: " d2
read -p "Diretório 3: " d3

x1=$(find "$d1" -type f -name "*.xls" | wc -l)
b1=$(find "$d1" -type f -name "*.bmp" | wc -l)
d1=$(find "$d1" -type f -name "*.docx" | wc -l)

x2=$(find "$d2" -type f -name "*.xls" | wc -l)
b2=$(find "$d2" -type f -name "*.bmp" | wc -l)
d2=$(find "$d2" -type f -name "*.docx" | wc -l)

x3=$(find "$d3" -type f -name "*.xls" | wc -l)
b3=$(find "$d3" -type f -name "*.bmp" | wc -l)
d3=$(find "$d3" -type f -name "*.docx" | wc -l)

echo -e "\nQuantidade de arquivos .xls em $d1: $x1"
echo "Quantidade de arquivos .bmp em $d1: $b1"
echo "Quantidade de arquivos .docx em $d1: $d1"

echo -e "\nQuantidade de arquivos .xls em $d2: $x2"
echo "Quantidade de arquivos .bmp em $d2: $b2"
echo "Quantidade de arquivos .docx em $d2: $b2"

echo -e "\nQuantidade de arquivos .xls em $d3: $x3"
echo "Quantidade de arquivos .bmp em $d3: $b3"
echo "Quantidade de arquivos .docx em $d3: $d3"
