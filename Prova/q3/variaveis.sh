#!/bin/bash

# 1. Criando variáveis no bash

#Atribuição direta
no="Clebson"
id=21

# Usando comandos para atribuir valores a variáveis
dt=$(date)
la=$(ls)

# 2.Diferença entre entrada do usuário e parâmentros de linha de comando
# Solicitar entrada do usuário
read -p "Digite seu nome: " nm

# Recebendo parâmetros de linha de comando
p1=$1
p2=$2

# 3. Variáveis automáticas

echo "Diretório atual: $PWD"
echo "Número de argumentos: $#"
echo "Numero de processo do shell: $$"
echo "Último comando executado: $HISTCMD"

# Exemplos de variáveis automáticas em ação
echo "Este script foi chamado com $# argumentos."
echo "O primeiro argumento é: $1"
echo "O segundo argumento é: $2"
