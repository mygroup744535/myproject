#!/bin/bash


echo "No silêncio da noite, as estrelas brilham."
sleep 2
echo "A lua no céu, um farol a guiar."
sleep 2
echo "O vento sussurra segredos antigos."
sleep 2
echo "Na escuridão, a esperança a florescer."
sleep 2
echo "As sombras dançam em torno de nós."
sleep 2
echo "Os sonhos se entrelaça, com a realidade."
sleep 2
echo "No coração, um fogo que nunca se apaga."
sleep 2
echo "A vida é uma jornada, sempre a continuar."
sleep 2
echo "Em cada passo, um novo começo."
sleep 2
echo "Com amor e coragem, enfrentaremos o amanhã."
