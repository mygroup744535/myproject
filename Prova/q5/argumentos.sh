#!/bin/bash

mkdir "$1"
echo "$1" > "$1/README.md"
date +%Y-%m-%d >> "$1/README.md"

mkdir "$2"
echo "$2" > "$2/README.md"
date +%Y-%m-%d >> "$2/README.md"

mkdir "$3"
echo "$3" > "$3/README.md"
date +%Y-%m-%d >> "$3/README.md"

mkdir "$4"
echo "$4" > "$4/README.md"
date +%Y-%m-%d >> "$4/README.md"
