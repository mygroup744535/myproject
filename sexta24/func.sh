#!/bin/bash

linha() {
    if [ $# -ne 2 ]; then
        echo "Uso: linha <numero_linha> <arquivo>"
        return 1
    fi

    num_lin=$1
    arq=$2

    if [ ! -f "$arq" ]; then
        echo "Arquivo não encontrado: $arq"
        return 1
    fi

    sed -n "${num_lin}p" "$arq"
}

coluna() {
    if [ $# -ne 2 ]; then
        echo "Uso: coluna <numero_coluna> <arquivo>"
        return 1
    fi

    num_col=$1
    arq=$2

    if [ ! -f "$arq" ]; then
        echo "Arquivo não encontrado: $arq"
        return 1
    fi

    cat "$arq" | cut -d: -f"${num_col}"
}

