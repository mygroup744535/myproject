#!/bin/bash

source func.sh

echo "Escolha entre linhas ou colunas (l/c):"
read opcao

case $opcao in
    l)
        linha "$1" "$2"
        ;;
    c)
        coluna "$1" "$2"
        ;;
    *)
        echo "Opção inválida. Escolha 'l' para linhas ou 'c' para colunas."
        ;;
esac
