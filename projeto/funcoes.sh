#!/bin/bash

function download_file() {
    local username
    local ip
    local src_file
    local dest_path
    local password

    # verifica se o sshpass está instalado
    if ! command -v sshpass &> /dev/null; then
        zenity --error --title="Erro" --text="Por favor, instale o sshpass e tente novamente"
        exit 1
    fi

    # solicita o nome de usuario
    username=$(zenity --entry --title="Transferência de Arquivos" --text="Digite o nome de usuario:")

    # solicita o ip
    ip=$(zenity --entry --title="Transferência de Arquivos" --text="Digite o endereço IP:")

    # solicita o caminho do arquivo de origem
    src_file=$(zenity --entry --title="Transferência de Arquivos" --text="Digite o caminho do arquivo na máquina remota:")

    # verifica se o usuário cancelou a seleção
    if [ "$src_file" = "" ]; then
        zenity --error --title="Erro" --text="Transferência cancelada pelo usuário"
        exit 1
    fi

    # solicita o caminho de destino
    dest_path=$(zenity --file-selection --title="Selecione o diretório de destino" --directory="$HOME")

    # verifica se o usuário cancelou a seleção
    if [ "$dest_path" = "" ]; then
        zenity --error --title="Erro" --text="Transferência cancelada pelo usuário"
        exit 1
    fi

    # solicita a senha
    password=$(zenity --password --title="Transferência de Arquivos" --text="Digite a senha:")

    # faz a transferencia de arquivos usando o scp e sshpass
    sshpass -p $password scp $username@$ip:$src_file $dest_path
}

function upload_file() {
    local username
    local ip
    local dest_path
    local src_file
    local password

    # verifica se o sshpass está instalado
    if ! command -v sshpass &> /dev/null; then
        zenity --error --title="Erro" --text="Por favor, instale o sshpass e tente novamente"
        exit 1
    fi

    # solicita o nome de usuario
    username=$(zenity --entry --title="Transferência de Arquivos" --text="Digite o nome de usuario:")

    # solicita o ip
    ip=$(zenity --entry --title="Transferência de Arquivos" --text="Digite o endereço IP:")

    # solicita o caminho de destino
    dest_path=$(zenity --entry --title="Transferência de Arquivos" --text="Digite o caminho do diretório na máquina remota:")

    # verifica se o usuário cancelou a seleção
    if [ "$dest_path" = "" ]; then
        zenity --error --title="Erro" --text="Transferência cancelada pelo usuário"
        exit 1
    fi

    # solicita o caminho do arquivo de origem
    src_file=$(zenity --file-selection --title="Selecione o arquivo de origem")

    # verifica se o usuário cancelou a seleção
    if [ "$src_file" = "" ]; then
        zenity --error --title="Erro" --text="Transferência cancelada pelo usuário"
        exit 1
    fi

    # solicita a senha  
    password=$(zenity --password --title="Transferência de Arquivos" --text="Digite a senha:")

    # faz a transferencia de arquivos usando o scp e sshpass
    sshpass -p $password scp $src_file $username@$ip:$dest_path
}
