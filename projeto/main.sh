#!/bin/bash
source funcoes.sh
# função para exibir o menu principal
while true;do
    # exibe o menu e solicita a opção escolhida
    option=$(zenity --list --title="Menu Principal" --column="Opções" "Download de Arquivos" "Upload de Arquivos")

    # Verifica se a opção escolhida é vazia (ou seja, o usuário clicou no botão de cancel)
    if [ -z "$option" ]; then
        exit 0
    fi 

    case $option in
        "Download de Arquivos")
            download_file
            ;;
        "Upload de Arquivos")
            upload_file
            ;;
    esac
done
