#!/bin/bash

wget https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log

head -n 1000 access.log > ips.txt
tail -n 1000 access.log >> ips.txt

rm access.log

ips=$(grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" ips.txt)

echo $ips.txt | tr ' ' '\n' | sort -u >> ip_o

for ip in $(cat ip_o); do
    ping -c 1 $ip
done
