#!/bin/bash

if [ -Z "$1" ]; then
	DIR="/home/ifpb"
else
	dir="$1"
fi

if [ $(whoami) != "ifpb" ];then
	echo "Deu ruim... :("
	exit 1;
fi

ls -F $1 | grep '/' | wc -l
ls -F $1 | grep -v '/' | wc -l
