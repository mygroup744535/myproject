#!/bin/bash

# Inicialize contadores
existentes=0
nao_existentes=0

# Loop para verificar a existência dos arquivos passados como parâmetros
for arquivo in "$@"; do
    if [ -e "$arquivo" ]; then
        existentes=$((existentes + 1))
    else
        nao_existentes=$((nao_existentes + 1))
    fi
done

# Imprimir resultados
echo "Quantidade de arquivos existentes: $existentes"
echo "Quantidade de arquivos não existentes: $nao_existentes"

