#!/bin/bash

# verifica se o usuário atual é ifpb
if [ "$(whoami)" != "ifpb" ]; then
	echo "você não é o usuário ifpb"
	exit 1
fi

#diretório a ser analisado ou /home/ifpb
dir="${1:-/home/ifpb}"

#define o diretório a ser usado
if [ ! -d "$dir" ]; then
	echo "o diretorio não existe"
	exit 1
fi

#conta a quantidade de arquivos e diretórios
num_arq=$(find "$dir" -type f | wc -l)
num_dir=$(find "$dir" -type d | wc -l)

# exibe as informções
echo "quantidade de arquivos em '$dir': $num_arq"
echo "quantidade de arquivos em '$dir': $num_dir"
 

