#!/bin/bash

# LISTA DE ALGUMAS VARIAVEIS AUTOMATICAS DO BASH

echo '$?:Imprime o código de retorno do último comando executado.'
echo '$-:Imprime as opçoes de configuração atualmente ativadas do bash.'
echo '$!:Imprime o PID (Process ID) do último comando no segundo plano (background) executado.'
echo '$@:Imprime todas as parâmetros da linha de comando como uma lista separada.'
echo '$#: Imprime o número de parâmetros na linha de comando.'
echo '$$: Imprima o PID do processo Bash atual.'
echo '$*: Imprima todas as configurações da linha de comando como uma única string.'
echo '$0: Imprima o nome do script atual.'
echo '$1: Imprima o primeiro parâmetro da linha de comando.'
echo '$2: Imprima o segundo parâmetro da linha de comando.'
echo '$3: Imprima o terceiro parâmetro da linha de comando, se estiver definido.'
echo '$4: Imprima o quarto parâmetro da linha de comando, se estiver definido.'
echo '$5: Imprima o quinto parâmetro da linha de comando, se estiver definido.'
echo '$6: Imprime o sexto parâmetro da linha de comando, se você estiver definido.'
echo '$7: Imprima o sétimo parâmetro da linha de comando, se estiver definido.'
echo '$8: Imprima o oitavo parâmetro da linha de comando, se estiver definido.'
echo '$9: Imprima ou não um parâmetro da linha de comando, se estiver definido.'
echo '$#: Esta linha imprime o número de parâmetros da linha de comando novamente. No entanto, esta é uma reprodução do número de parâmetros para lembrar quantos parâmetros foram passados para o script.'
echo '$*: Esta linha imprime todas as configurações da linha de comando como uma única string. Os parâmetros são concatenados em uma única string com espaços entre eles.'
echo '$@: Semelhante à linha anterior, esta imprime todas as configurações da linha de comando, mas mantém cada parâmetro como uma palavra separada. Cada parâmetro é tratado como uma palavra separada.'
