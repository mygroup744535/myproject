#!/bin/bash

dir1=$1
dir2=$2

#lista os arquivos do primeiro diretório e redireciona a saída para arquivo o3.txt
ls "$dir1" > o3.txt

# Insere a linha de separação
echo "- - - - - - - - - - - - - - - -" >> o3.txt

# Lista os arquivos do segundo diretório e anexa a saída do arquivo o3.txt

ls "$dir2" >> o3.txt
