#!/bin/bash

for i in {1..3}; do
	num=$((RANDOM % 12 + 2))

	nome="${num}.txt"

        if [ -e "$nome" ]; then
		echo " Já existe um arquivo com nome $nome"
                
	else
		touch "$nome"
	fi
done
	echo "Arquivo $nome criado."	
