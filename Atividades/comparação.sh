#!/bin/bash


n1=$1
n2=$2

test -e $n1 && echo $1 existe || exit 1
test -e $n2 && echo $1 existe || exit 1

numero_linha1=$(wc -l < $n1)
numero_linha2=$(wc -l < $n2)

test $numero_linha1 -gt $numero_linha2 && echo $n1 possui mais linhas || echo $n2 possui mais linhas
