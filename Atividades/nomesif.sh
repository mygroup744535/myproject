#!/bin/bash

read -p "nome 1: " n1
read -p "nome 2: " n2

if [ -z "$n1" ] || [ -z "$n2" ]; then
	exit 1

else
	[ "$n1" < "$n2" ] && echo $n1 $n2 || echo $n2 $n1

fi

