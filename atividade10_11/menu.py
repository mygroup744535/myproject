#!/bin/bash

while [ "$op"!="exit" ]; do
    echo "a -> executar ping"
    echo "b -> Listar usários logados na máquina"
    echo "c -> Exiber o uso de memória e de disco na máquina"
    echo "d -> sair"

    read -p "digite uma opção:" op

    if [ "$op" == "a" ]; then
        read -p "digite o endereço"  ip
        ping -c 5 $ip
    fi

    [ "$op" == "b" ] && who

    [ "$op" == "c" ] && free && du

    [ "$op" == "d" ] && break

done

